FROM frolvlad/alpine-glibc

ENV HOME=/home/teamspeak
RUN adduser -D -s /bin/false -h $HOME -u 2000 teamspeak
   
COPY startserver.sh /home/startserver.sh
RUN chown teamspeak:teamspeak /home/startserver.sh && chmod +x /home/startserver.sh
USER teamspeak
VOLUME $HOME
WORKDIR $HOME

EXPOSE 9987/udp 10011 30033
ENTRYPOINT ["/home/startserver.sh"]
