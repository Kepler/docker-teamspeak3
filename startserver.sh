#!/bin/ash
TS_VERSION="3.0.13.6"
echo "Testing if we need to download"
cd /home/teamspeak

TARFILE=teamspeak3-server_linux_amd64-${TS_VERSION}.tar.bz2

if [ ! -d server ]; then
  mkdir server
fi

if [ ! -e ${TARFILE} ]; then
  echo "Downloading ${TARFILE} ..."
  wget -q http://dl.4players.de/ts/releases/${TS_VERSION}/${TARFILE} \
  && tar xjf ${TARFILE} && cp -r teamspeak3-server_linux_amd64/* server/ && rm -rf teamspeak3-server_linux_amd64
fi

export LD_LIBRARY_PATH=/home/teamspeak/server

TS3ARGS=""
if [ -e /home/teamspeak/server/ts3server.ini ]; then
  TS3ARGS="inifile=/home/teamspeak/server/ts3server.ini"
else
  TS3ARGS="createinifile=1"
fi
cd server
exec ./ts3server $TS3ARGS
