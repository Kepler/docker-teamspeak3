# How to use
Pull this repository.  
Modify docker-compose.yml to suit your needs.  
Modify TS_VERSION in the file startserver.sh if you want another server version.  
Create the directory mapped in docker-compose.yml and set userID to 2000 for this directory.  

## First time usage
run *docker-compose up --build* to build and run image.  
Copy the essential information from the teamspeak server output.  
Press CTRL-C.  

## Get the server running again
run *docker-compose up -d* or *docker start <containerId>*.